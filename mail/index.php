<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
    <!-- Web Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Josefin Slab' rel='stylesheet'>
    <!-- Favicon -->
    <link type="image/ico" rel="shortcut icon" href="../favicon.ico">
    <link type="image/ico" rel="icon"  href="../favicon.ico">
    <!-- Title -->
    <title>Subscribe to KateForrestAuthor.com</title>
    <!-- Style Sheet -->
    <link rel="stylesheet" href="mail-style.css" media="none" onload="if(media!='all')media='all'">
    <!-- Schema Tags -->
    <script type="application/ld+json">
        {
            "@context": "http://schema.org",
            "@type": "WebSite",
            "url": "https://kateforrestauthor.com/mail/",
            "name": "Kate Forrest",
            "mainEntityOfPage": "https://kateforrestauthor.com/mail/index.php",
            "headline": "Subscribe to KateForrestAuthor.com",
            "about": "Kate Forrest is the author of The Crusader's Heart, a Scottish historical romance novel.",
            "author": {
                "@type": "Person",
                "name": "Kate Forrest"
            },
            "dateCreated": "2018-07-24",
            "datePublished": "2018-07-25",
            "dateModified": "2025-02-07",
            "image": {
                "@type": "ImageObject",
                "url": "https://kateforrestauthor.com/photos/profile_250x250.jpg",
                "height": 250,
                "width": 250
            }
        }
    </script>
</head>
<body itemscope itemtype="https://schema.org/WebPage">
    <?php
        // PHP Form Code

        // Declaration & Initialization
        $my_mail = $my_name = "";
        $nameErr = $emailErr = "";
        $is_success = false;

        // Checks
        if ($_SERVER["REQUEST_METHOD"] == "POST"){
            if (empty($_POST["name"])) {
                $nameErr = "Name is required";
            } elseif ($_POST["message"] != "") {
                $nameErr = "Oops. You are a bot.";
            } else {
                $my_name = test_input($_POST["name"]);
                // check if name only contains letters and whitespace
                if (!preg_match("/^[a-zA-Z \.-]*$/",$my_name)) {
                    $nameErr = "Only letters, dashes, dots and white space allowed";
                }
            }

            if (empty($_POST["email"])) {
                $emailErr = "Email is required";
            } else {
                $my_mail = test_input($_POST["email"]);
                // check if e-mail address is well-formed
                if (!filter_var($my_mail, FILTER_VALIDATE_EMAIL)) {
                    $emailErr = "Invalid email format";
                }
            }

            // Check for no errors
            if ($nameErr === "" and $emailErr === "") {
                $email_from = "kforrest@kateforrestauthor.com";
                $email_bcc = "Kate.Forrest.SL5@gmail.com";
                $email_subject = "Subscribe to kateforrestauthor.com";
                $email_body = "Hi $my_name.\nTo subscribe to the kateforrestauthor.com mailing list, please reply to this email. Thank you for your interest.\nSincerely,\nKate Forrest\n\n\n\n\nThis message was sent to $my_mail. Please delete this message if you are not the intended recipient.\nTo unsubscribe, go to https://kateforrestauthor.com/mail/unsubscribe.php";
                $to = "$my_mail";
                $headers = "From: $email_from \r\n";
                $headers .= "Reply-To: $email_from \r\n";
                $headers .= "Bcc: $email_bcc \r\n";

                // Send email
                mail($to,$email_subject,$email_body,$headers);
                $is_success = true;
            }
        }

        // Functions
        function test_input($data) {
            // Ref: https://www.w3schools.com/php/php_form_validation.asp
            $data = trim($data);
            $data = stripslashes($data);
            $data = htmlspecialchars($data);
            return $data;
        }
    ?>

    <!-- HTML Form Code -->
    <!-- Sends php data to itself, scrubbing injection code, if any -->
    <div class="top-area">
        <h1 class="josefin">Subscribe to KateForrestAuthor.com</h1>
        <div class="<?php if($is_success) {echo "noshow";} else {echo "section1";}?>">
            <div class="section1-text josefin" style="font-size: 14pt;">
                Join the mailing list to receive updates on upcoming books and events, but never any spam.
            </div>
            <br />
            <div class="section1-text josefin" style="font-size: 11pt; padding: 1em 1em;">
                By submitting your email address, you agree to our Privacy Policy.
                We will use your information to send periodic updates on new releases and upcoming events.
                You may change your mind at any time by entering your email to the <a href="https://kateforrestauthor.com/mail/unsubscribe.php" target="_blank">unsubscribe</a> page.
            </div>
            <div class="subform tree">
                <div class="subform-left">
                    <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
                        <input type="text" name="name" id="user-name" class="subinput" placeholder="Name ..." value="<?php echo $my_name;?>">
                        <span class="error">* <?php echo $nameErr;?></span>
                        <input type="text" name="email" id="user-email" class="subinput" placeholder="E-Mail ..." value="<?php echo $my_mail;?>">
                        <span class="error">* <?php echo $emailErr;?></span>
                        <div class="form1">
                            <input type="text" name="message" id="user-message" placeholder="Type nothing here." value="">
                        </div>
                        <br><br>
                        <input type="submit" name="submit" class="button-submit" value="Join">
                    </form>
                </div>
            </div>
        </div>
        <div class="<?php if ($is_success) {echo "section1";} else {echo "noshow";}?>">
            <div class="section1-text josefin" style="font-size: 14pt;">
                <p>Thank you for your submission.
                An email has been sent to the address you provided.</p>
                <p style="font-weight: bold; color: red;">To complete the process, you must reply to the message that was sent you.</p>
                <p>Please add <b>kforrest@kateforrestauthor.com</b> to your contacts to ensure the message reaches your inbox.
                Otherwise, you may need to check your spam folder.
                Thank you!</p>
            </div>
            <br><br>
            <div class="section1-text josefin" style="font-size: 16pt;">
                You may close this window now.
            </div>
        </div>
    </div>
    <div class="footer">
        <p>KateForrestAuthor.com<br />&copy; 2025 | All rights reserved.</p>
        <ul class="footer-list">
            <li class="footer-social">
                <a href="https://www.instagram.com/kateforrestauthor" target="_blank" class="icon-space">
                    <svg class="footer-icon" xmlns="http://www.w3.org/2000/svg" width="24" height="28" viewbox="0 0 24 28">
                        <title>Instagram</title>
                        <path d="M16 14c0-2.203-1.797-4-4-4s-4 1.797-4 4 1.797 4 4 4 4-1.797 4-4zm2.156 0c0 3.406-2.75 6.156-6.156 6.156S5.844 17.406 5.844 14 8.594 7.844 12 7.844s6.156 2.75 6.156 6.156zm1.688-6.406c0 .797-.641 1.437-1.437 1.437S16.97 8.39 16.97 7.594s.641-1.437 1.437-1.437 1.437.641 1.437 1.437zM12 4.156c-1.75 0-5.5-.141-7.078.484-.547.219-.953.484-1.375.906s-.688.828-.906 1.375c-.625 1.578-.484 5.328-.484 7.078s-.141 5.5.484 7.078c.219.547.484.953.906 1.375s.828.688 1.375.906c1.578.625 5.328.484 7.078.484s5.5.141 7.078-.484c.547-.219.953-.484 1.375-.906s.688-.828.906-1.375c.625-1.578.484-5.328.484-7.078s.141-5.5-.484-7.078c-.219-.547-.484-.953-.906-1.375s-.828-.688-1.375-.906C17.5 4.015 13.75 4.156 12 4.156zM24 14c0 1.656.016 3.297-.078 4.953-.094 1.922-.531 3.625-1.937 5.031s-3.109 1.844-5.031 1.937c-1.656.094-3.297.078-4.953.078s-3.297.016-4.953-.078c-1.922-.094-3.625-.531-5.031-1.937S.173 20.875.08 18.953C-.014 17.297.002 15.656.002 14s-.016-3.297.078-4.953c.094-1.922.531-3.625 1.937-5.031s3.109-1.844 5.031-1.937c1.656-.094 3.297-.078 4.953-.078s3.297-.016 4.953.078c1.922.094 3.625.531 5.031 1.937s1.844 3.109 1.937 5.031C24.016 10.703 24 12.344 24 14z"></path>
                    </svg>
                </a>
            </li>
            <li class="footer-social">
                <a href="https://www.facebook.com/kateforrestauthor/" target="_blank" class="icon-space">
                    <svg class="footer-icon" xmlns="http://www.w3.org/2000/svg" width="16" height="28" viewbox="0 0 16 28">
                        <title>Facebook</title>
                        <path d="M14.984.187v4.125h-2.453c-1.922 0-2.281.922-2.281 2.25v2.953h4.578l-.609 4.625H10.25v11.859H5.469V14.14H1.485V9.515h3.984V6.109C5.469 2.156 7.891 0 11.422 0c1.687 0 3.141.125 3.563.187z"></path>
                    </svg>
                </a>
            </li>
            <li class="footer-social">
                <a href="mailto:Kate.Forrest.SL5@gmail.com?&subject=Regarding KateForrestAuthor.com&body=Hi%20Kate%20Forrest.%20I%20visited%20your%20website%20and%20wanted%20to%20connect." target="_blank" class="icon-space">
                    <svg class="footer-icon" xmlns="http://www.w3.org/2000/svg" width="28" height="28" viewBox="0 0 1792 1792">
                        <title>Email</title>
                        <path d="M1792 710v794q0 66-47 113t-113 47h-1472q-66 0-113-47t-47-113v-794q44 49 101 87 362 246 497 345 57 42 92.5 65.5t94.5 48 110 24.5h2q51 0 110-24.5t94.5-48 92.5-65.5q170-123 498-345 57-39 100-87zm0-294q0 79-49 151t-122 123q-376 261-468 325-10 7-42.5 30.5t-54 38-52 32.5-57.5 27-50 9h-2q-23 0-50-9t-57.5-27-52-32.5-54-38-42.5-30.5q-91-64-262-182.5t-205-142.5q-62-42-117-115.5t-55-136.5q0-78 41.5-130t118.5-52h1472q65 0 112.5 47t47.5 113z"></path>
                    </svg>
                </a>
            </li>
            <!-- Account not created yet
            <li class="footer-social">
                <svg class="footer-icon" xmlns="http://www.w3.org/2000/svg" width="26" height="28" viewBox="0 0 26 28">
                    <title>Twitter</title>
                    <path d="M25.312 6.375a10.85 10.85 0 0 1-2.531 2.609c.016.219.016.438.016.656 0 6.672-5.078 14.359-14.359 14.359-2.859 0-5.516-.828-7.75-2.266.406.047.797.063 1.219.063 2.359 0 4.531-.797 6.266-2.156a5.056 5.056 0 0 1-4.719-3.5c.313.047.625.078.953.078.453 0 .906-.063 1.328-.172a5.048 5.048 0 0 1-4.047-4.953v-.063a5.093 5.093 0 0 0 2.281.641 5.044 5.044 0 0 1-2.25-4.203c0-.938.25-1.797.688-2.547a14.344 14.344 0 0 0 10.406 5.281 5.708 5.708 0 0 1-.125-1.156 5.045 5.045 0 0 1 5.047-5.047 5.03 5.03 0 0 1 3.687 1.594 9.943 9.943 0 0 0 3.203-1.219 5.032 5.032 0 0 1-2.219 2.781c1.016-.109 2-.391 2.906-.781z"></path>
                </svg>
            </li>
            -->
        </ul>
        <p class="footer-text-sm">Watercolor designs provided by <a href="https://www.etsy.com/shop/PikakePress" target="_blank" class="footer-url-text">Pikake Press</a>.</p>
    </div>
</body>
</html>
