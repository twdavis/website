#!/usr/bin/perl
#
# exifread.pl
# written by Tyler W. Davis
#
# 2010-03-09 -- created
# 2019-03-17 -- last updated
#
# HOW TO RUN:
# > perl exifread.pl
#
# ------------
# description:
# ------------
# This script reads a directory for JPG files, reads their exif tags for
# GPS coordinates, and outputs a string formatted for GIS
#
# \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
# LOAD MODULES:
# ////////////////////////////////////////////////////////////////////////////
use strict;
use warnings;
use File::Basename;
use Image::ExifTool qw(ImageInfo);

# \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
# MAIN
# ////////////////////////////////////////////////////////////////////////////
# Initialize script variables:
my $dir = ".";

# Open directory and read all files:
# NOTE: change extension in grep based on needs
#       currently reads all jpg files
opendir(MYDIR, $dir) or die $!;
my @files = grep {/.*\.{1}jpg$/} readdir MYDIR;
closedir(MYDIR);

# Print warning if no files found
if ( @files < 1 ) {
    print "0 files found\n";
}

my $id = 0;

# The website URL for this set of images:
my $day = 2;
my $weburl = "https://kateforrestauthor.com/story/map/2018/$day/";

# Print header lines
my $headerline = "ID,LAT,LON,DAY,PHOTO,THUMB";
print "$headerline\n";

# Read through each file name you found and rename it appropriately:
for my $name (sort(@files)) {
    ++$id;
    chomp($name);

    # Create the thumb name:
    my $bname;  # basename w/o file extension
    my $fpath;  # file path (should be local './')
    my $fextn;  # file extension (should be .jpg)
    ($bname, $fpath, $fextn) = fileparse($name, qr/\.[^.]*/);
    my $tname = sprintf("%s_THUMB%s", $bname, $fextn);

    # Read GPS location using ExifTool
    # WARNING: does not handle errors w/ exif codes
    my $exifTool = new Image::ExifTool;
    my $info = $exifTool->ImageInfo($name);
    my $lat = $$info{"GPSLatitude"};
    my $lon = $$info{"GPSLongitude"};
    my $ddlat = dms2dd($lat);
    my $ddlon = dms2dd($lon);

    print "$id,$ddlat,$ddlon,$day,$weburl$name,$weburl$tname\n";
}

# \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
# SUBROUTINES
# ////////////////////////////////////////////////////////////////////////////
sub dms2dd {
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # Title: dms2dd
    # Inputs: string coordinates in DMS format (e.g., 0 deg 0' 0.00" N)
    # Output: string coordinates converted to decimal degrees
    # WARNING: does not handle errors, returns a zero instead
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    my $dms = $_[0];
    my $reg = qr/(\d{1,2})\sdeg\s(\d{1,2})'\s(\d{1,2}.\d{1,2})"\s([NESW])/;
    my $dd = 0.0;
    if ($dms =~ /$reg/) {
        # Correct +/- for south and west coordinates
        if ($4 eq 'S' || $4 eq 'W'){
            $dd = -1.0*(1.0*$1 + $2/60.0 + $3/3600.0);
        } else {
            $dd = 1.0*$1 + $2/60.0 + $3/3600.0;
        }
    }
    else {
        print "No match\n";
    }
    return $dd;
}
