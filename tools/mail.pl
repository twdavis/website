#!/usr/bin/perl
#
# mail.pl
#
# Written by Tyler W. Davis
# 2018-09-23 --- created
# 2018-09-26 --- updated
#
#
# This script e-mails an HTML file as a MIME attachment using SMTP to a list
# of recipients.
#
#
# Changelog
# - changed host to domain name [18.09.26]
# - updated username for from addr [18.09.26]
# - changed csv columns [18.09.26]
# - created sendnews subroutine [18.09.24]
# - added read csv for mail addresses
#
# TODO:

use strict;
use warnings;
use MIME::Lite;

# Read credential file
open (FILE, "<credfile.txt") or die $!;
my $line = <FILE>;
my @creds = split(' ', $line);
close FILE;
my $username = $creds[0];
my $password = $creds[1];


# Read HTML into Perl scalar:
my $html_file;
$html_file = '../newsletter/2018/09/index.html';
my $html = do { local( @ARGV, $/ ) = $html_file; <> };

# Read mail list addresses:
open (FILE, "<mail_list.csv") or die $!;
my @maddresses = ();
while (my $line = <FILE>) {
    chomp($line);
    my @values = split(',', $line);
    push(@maddresses, $values[2]);

    print "Sending to " . $values[2] . "\n";
    &sendnews($values[2], $html, $username, $password);
    sleep 3;
}

# Define subroutine
sub sendnews {
    # Define variables
    my($to_addr, $content, $user, $pass) = @_;
    my $msg;

    # Prepare HTML message
    $msg = MIME::Lite->new(From    => $user,
                           To      => $to_addr,
                           Subject => "Kate Forrest (Davis) Releases Debut Romance Novel, The Crusader's Heart",
                           Type    => 'multipart/mixed');

    $msg->attach(Type        => 'text/html',
                 Data        => $content);

    # Send email
    $msg->send('smtp', 'kateforrestauthor.com',
                SSL => 1,
                AuthUser => $user,
                AuthPass => $pass,
    );
}

__END__
