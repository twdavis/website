---
title: "Becoming Something"
subtitle: "Week 6 in the Garden"
author: "Kate Forrest"
date: "April 6, 2021"
abstract: "We put another productive week in the books. We tackled our big project of building a retaining wall for our Pear Tree Bed. I'm so happy we decided to use flagstone retaining wall blocks---it looks fantastic! We also got some seeds sown out, daffodils transplanted, supports hung for the sweet peas, and some perennials potted up and planted out. Let's jump in!"
keywords:
- "Blog"
- "Garden"
url: 2021-04-06-becoming-something.html
ishomepage: false
---

> "A garden, hovering always in a state of becoming, sums its own past and its future." <br />&mdash; Bunny Mellon

Hi all!
We've put another productive week in the books.
We tackled our big project of building a retaining wall for our Pear Tree Bed.
I'm so happy we decided to use flagstone retaining wall blocks---it looks fantastic!
We also got some seeds sown out, daffodils transplanted, supports hung for the sweet peas, and some perennials potted up and planted out.
Let's jump in!

I actually thought to take notes daily, as opposed to writing this all at once.
I only got around to doing that one day last week (it'll take me awhile to start a new routine), so here's my one present tense entry.
The rest will be a recap that I wrote today.

## Frost Worries

### Wednesday, March 31<sup>st</sup>

I'm sitting in my office and it's about 3:00 in the afternoon.
I've got my windows open because it is 73 degrees outside, but we've got rain starting any minute and a cold front sweeping in that'll give us lows Thursday and Friday night in the upper 20s.
I'm not super worried about any of the plants we're keeping in the greenhouse---they're all cold hardy perennials and/or dormant (like the hostas I just put in pots), but I am concerned about some of the tender foliage coming in on some of our summer flowering plants, like the perennial lantana and pineapple sage.

We're good for the night (low in the mid 40s), but tomorrow afternoon I want to get some leaves or mulch on them to protect the new growth.
Fortunately, by Easter Sunday we're looking at highs back in the 70s and lows in the upper 40s to 50s going into next week, so fingers crossed this is the last big cold snap of the spring.
Our average last frost is around April 10<sup>th</sup>, which is why I'm glad I haven't jumped into buying tender plants yet.
Last year, with the pandemic starting, I felt hysterical about plant availability (and honestly, rightly so, because I think a lot of us took to gardening to get through these extreme times), so I bought tomatoes way too early and spent a lot of time wheeling them in and out of the house until I felt safe putting them in the garden.
Anyway, we'll see what happens this week.

## Our Plants Survived!

Saturday we uncovered everything and only had one casualty: a daffodil in our backyard.
Otherwise, everything did just fine.
After checking on all the plants and uncovering everything that we protected, it was time for the big task for the weekend.

## The Retaining Wall

We had the blocks delivered Wednesday by truck, which meant we got our first pallet---very exciting!
We've been wanting pallets for my husband to build us a new composting system.
I think we'll need seven in total, so one down and six to go.
Anyway, we had it delivered to our driveway but needed to move them to the other side of our property, so that was a timely endeavor because our wheelbarrow only had room for about four blocks at a time.

We went with a concrete flagstone looking retaining wall block from Lowe's (size 11-in L &times; 4-in H &times; 6-in D).
We ordered 50 and only used 38, because we originally envisioned the whole bed being surrounded by the blocks.
Once we started working on it, we realized it would look nicer to finish on a half-circle around the pear tree, with the blocks leveling out with the ground, as opposed to taking the blocks around to the house.
At the lowest point, near our front stoop where the bed dramatically dips, we did three levels (12 inches tall), then half of the wall was two levels (8 inches tall), before we finished with just one level (4 inches tall) for the half-circle.
My husband leveled out the ground and used sand as the base for the blocks.
I must say, it looks like a professional job.
It really transformed that bed into a feature for the front of the house.

![The new retaining wall.](assets/images/wall_210406_900x438.png)

It doesn't mirror the bed on the other side of the stoop perfectly, since the Lantana bed has actual flagstone, but the concrete blocks are molded to resemble natural stone and the coloration while called "brown" is a blend of tan and gray streaks, which is similar in color to the flagstone.
There was also a substantial cost difference: our 50 retaining wall blocks cost \$115 (plus \$60 for truck delivery; so worth it!), while a genuine flagstone wall would have cost us between \$1500 to \$2000.
It also took us four hours and I think even if we had gone higher and done another level or two, it wouldn't have added that much time.
Really the bulk of the effort goes into leveling that first layer.
Once you've got that done right, everything goes pretty quickly.

We then added more soil to the bed, but we didn't top it off with mulch because Sunday we sowed alyssum seeds and forget-me-nots in the front of that bed.
The alyssum requires sun for germination, so once those get started and everything gets a few inches tall, we'll go back and finish mulching it.

## Easter Sunday

<div class="img-fr-wrapper">
![Peas.](assets/images/peas_210406_500x667.png)
</div>

We started the day with a stroll at Colonial Williamsburg.
We got over there later than I wanted (after 10:00 AM) and it was already bustling with people.
There were a few gardens I wanted to see off Duke of Gloucester Street, but only got to do a handful as I wasn't feeling 100%.
I'd gotten my first COVID vaccine Friday, so I was a bit sore and fatigued still on Sunday, but I still got in a nice 30-minute walk with my husband in the gardens before we headed home for a big brunch to fuel an afternoon in the yard.

Initially, I imagined Sunday would be a light day of yardwork, but we ended up putting in a solid six hours outside.

We started by adding in support lines for the peas, a task undertaken by the engineer.
It was meticulously done.

## Lantana Bed

We also moved a Lithodora to make way for our new "Snowflake" creeping phlox.
I wanted white flowering phlox for on top of our flagstone retaining wall last year and had to settle for the Lithodora (also a beautiful ground cover, but it's blue).
However, to transplant the Lithodora we had to dig up and relocate about 30 daffodils crammed into a one-foot by one-foot area in our Lantana Bed.
We've been slowly digging these up and adding them to the bulb bed (the weeping redbud bed) in our front yard, but it's a time-consuming process to separate and replant them.
By this point it was also super sunny and in the mid 70s, so it was almost getting too uncomfortable to be working out front, but my husband got everything transplanted while I hid in the shade of the magnolia.
When that was done, we headed back over to the flagstone wall and planted in the phlox and catmint by our knockout rose.
We don't have a lot of real estate for planting in that corner of this bed because there was a redtip tree there and the stump and root system is still there.
I plan to add a few inches of topsoil around the stump and see if I can get some drought tolerant annuals for that corner. Likely marigolds.

I love the combination of the phlox, catmint, and rose bush and it feels very cottage gardenlike, so I'm calling that my "Cottage Garden Corner."
I haven't decided yet what all we'll plant out in the Lantana Bed this year.
I don't really have a vision for it.
It gets full sun and we've got hot summers here, so I want to use heat and drought tolerant plants in front of the Lantana bushes.
I'll likely incorporate pentas, because they did exceptionally well last year, and celosia (that I'll start from seed later this month or maybe even in May.
I like having it as a late bloomer going into fall).
I'm also a sucker for impulse plant buys, so if I see stuff something that catches my eye, it'll probably go in there.

## What's doing around our yard and Williamsburg?

I'm not going to do a full bloom list this week, but I'd say the shining stars at our property and around town (including Colonial Williamsburg) are the tulips and the redbud trees.

![Colonial Williamsburg's Governor's Palace Gardens.](assets/images/cw_210406_900x435.png)

I also spotted blooming wisteria off Monticello Ave in town today and we saw our very first butterfly of the season tonight as we worked in the yard.

Some final thoughts for the day... It's strange to already see the spring season waning.
Last year, I feel like we got a few extra weeks of spring, so it is bittersweet to move on to the next season.
I am, of course, excited to see the summer garden taking shape as we lay the groundwork for beautiful and dynamic displays in all our beds.
To pull from the Bunny Mellon quote, it is wonderful to see our garden evolving---becoming---as we move into the year.
The peas are growing, our onions are in the ground, and the strawberry plant is flowering.
Soon we'll be harvesting food from our very own garden and watching our flowers transform our landscape into a pollinator's paradise.
What a joy it is to be a gardener.

I'm running out of time to recap all our projects and our plant shopping from today, so I'll include my plant haul in next week's blog entry.
Until then, thanks for reading and following our garden adventures!
More next Tuesday, April 13<sup>th</sup>.

\~Kate
