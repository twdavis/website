---
title: "When in Windsor"
subtitle: "A writer's travel guide to Windsor, UK"
author: "Kate Forrest"
date: "March 14, 2020"
abstract: "This blog is a comprehensive guide to visiting Windsor, UK with a focus on Windsor Castle and Windsor Great Park. I lived near Windsor for two years (2013 to 2015) and I returned for a visit in the summer of 2018. This information is based on my knowledge and experience and I try to touch on important basics like parking and visiting for a day trip. I also go into detail about exploring Windsor Great Park, which wasn't even on my radar when I visited England for the first time, but I wish it had been. These are economical tips and suggestions for travelers that like the great outdoors, nature, gardens, and history."
keywords:
- "Travel"
- "Blog"
- "UK"
- "StoryMap"
- "Windsor"
- "Castle"
- "Great Park"
url: 2020-03-14-when-in-windsor.html
ishomepage: false
---

This article is a companion to my UK Story Map, which you can find [here][UKSM]. It isn't necessary to view the Story Map, but if you have time, I encourage you to view the Story Map entry for Day 2, Wednesday May 30<sup>th</sup>, which provides context for this information.

The purpose of this blog is to share travel tips, recommendations, and experiences from our nearly month-long trip to the United Kingdom in the summer of 2018. (Yes, I'm late to sharing this, but it's been a busy year and a half for us). My entries will be informed by our experience living in England for two years (2013 to 2015). We lived in Sunninghill, part of Ascot, which is about eight miles from Windsor. I went there nearly every week to shop and sight-see over those two years, so that's where I'll start this blog. Windsor is also where we began our trip when we visited in 2018.

**What will this blog cover?** The following are some tips and recommendations I have for visiting Windsor, Eton, and Windsor Great Park.

What you need to know about me to see to understand where this information is coming from:

**Travel budget:** Medium/Low. We'll pay for a couple of amusements, but we operate on a tight budget, so a lot of what we do is free or cheap.

**Travel style:** I love gardens, walking tours, history and drinking a good ale at the end of the day. My husband is happy to be along for the ride (which means I pretty much plan everything and that suits him). We do not do adventure tourism. We do, however, love nature, so we did a lot of out-of-doors activities. We are not foodies. Though we enjoy good food, fine dining is not in our budget and I would not categorize this as a culinary tour. However, in my [Story Map][UKSM], I do cover our daily dining experiences in my journal entries. What you'll find here is budget friendly approaches to dining.

**My travel in England:** We lived in England for two years (2013--2015) and during that time we tried to see most of the big sights in London (Westminster Abbey, Tower of London, Buckingham Palace) and visit popular areas within London's reach (Bath, Stonehenge, Salisbury, and the Cotswolds). Therefore, when we want back in 2018, we didn't tour many of the big sights and amusements. We did revisit some, but we didn't necessarily tour them. For example, I've toured the inside of Windsor Castle eight times, so we skipped it our return trip. Instead, we shopped and dined on Peascod Street, walked the Long Walk, and took the [Windsor Eton Heritage Trail][WEHTrail]. That being said, I will discuss Windsor Castle and, in subsequent entries, discuss the major sights in the areas we visited. As with most travel blogs, I think there will be some information that is helpful and relevant to you, no matter your travel style. If you are searching for information on a particular subject (dining, parking, Windsor Great Park), I have categorized each section, starting with "Cheap Eats."

Disclaimer: As always, check directly with the main source for current information on access and opening times, like Windsor Castle, to avoid disappointment when planning your visit.

## Cheap Eats

If you are on a budget and you don't want to sit for a leisurely meal, there are options for food on the go. Waitrose grocery store, which you'll find at King Edward Court behind Windsor Royal Shopping, has sandwiches and drinks to go as does Marks & Spencer (M&S) off Peascod Street. M&S is a department store with a wonderful food hall. Walk past the clothes and accessories on the first floor and you'll find it. Grab a sandwich, crisps, a banana or apple, and you've got lunch. Breakfast and tea-time treats? Sugared donuts in the bakery or scones with strawberry jam and cream (from the refrigerated foods area) are pretty great. When I'm there, I also grab a bag of Percy Pig pals (sweet gummy candies) and have picked up biscuit and tea tins to give as gifts to family and friends.

## Toilets

There are several public conveniences (rest rooms) in Windsor and Eton. Note that you'll find when traveling around the UK, that most towns have public restrooms. One of the easiest ones to find in Windsor is off High Street at the Guildhall. The entrances are down steps off the front of the building and there is signage for them. There are also rest rooms at the Victoria Street and River Street car parks and the Home Park, off Romney Lock Road. Most should be open until 6:00 PM or later daily, but if you want to confirm times and locations in advance of your trip, I recommend checking out the [Borough website][RBWM]. Most train stations also have public toilets but, for example, where we previously caught the train in Sunningdale (off the Reading to London Waterloo line), they had closed the toilets for "anti-social behavior." I'm not sure if this is still the case, but it's something to be aware of.

I've heard from folks that are apprehensive about travel abroad because they worry about finding access to public facilities. It honestly has never been a problem for us, even when driving in remote areas of Scotland. If you can't find a designated facility, stop into a pub or restaurant. Most are happy to allow you to use their facilities, even if you don't purchase anything. Don't let fear of not finding a place for a bathroom break hinder your travel---you will find a toilet!

## Parking

Parking sucks, as is the norm in most medieval towns in the UK. If you can, take a bus. If not, there are large car parks both in the town center and just outside. I don't recommend the one by Waitrose---it is insanely tight parking and even driving between levels was too narrow for comfort. I've never found success street parking, but there is always the exception to the rule. To me, it's easier to just pay and park in a lot away from the town, like King Edward VII carpark or the Home Park (both are past the Windsor & Eton Riverside Railway Station). As with all car parks, hide valuables or take them with you. We never had a break-in once in our two years living in the UK or during our trip in the summer of 2018. However, you should be mindful of the issue as you would be traveling anywhere.

## Accommodations

I don't have much advice on finding accommodations in Windsor or Eton because when we visited in the summer of 2018, I could not find any Air BnB listings that fit our budget, so we stayed in South Ascot. I talk about our lodging there at the start of my [UK Story Map][UKSM]. I should note that when we returned to the Windsor area at the end of our trip, the dates of our stay overlapped with Royal Ascot (a week of horse racing that the royal family attends every June). This meant Windsor, Ascot, and everything in the surrounding area was booked up or more expensive because of the big event. Therefore, one tip I do have when planning your vacation is to check out the events in the area. If [Royal Ascot][RoyalAscot] is on, chances are accommodations will book early and may be pricier.

## Can I make it a day trip from London?

Yes! Visiting Windsor Castle is a great day trip from London, so if you are staying in the big city and just want to take the train out for the day, that's a great option. You can take direct trains from London Waterloo to Windsor & Eton Riverside Station (down the hill from the castle) or you can go via Slough and come in to Windsor & Eton Central (which brings you in at the Royal Shopping center on top of the hill).

There are also plentiful bus tours from London that include Windsor Castle in their day-trip itineraries. My husband and I did one as newlyweds a decade ago that included Windsor, Bath, and Stonehenge. It's a great option if you want the comfort of a guided tour and don't mind sticking to a timetable.

## Windsor Castle

<div class="img-fr-wrapper">
![Windsor Castle-Long Walk. Image credit: Gambitek at Polish Wikipedia.][WindsorCastleJPG]
</div>

I want to talk about the main attraction in Windsor. Pictured is Windsor Castle, the magnificent medieval anchor of this splendid town. The castle was originally built by William the Conqueror in the 11<sup>th</sup> century. Today it is considered the oldest, and largest, inhabited castle in the world. Queen Elizabeth II spends most of her weekends at this favorite home and it is one of her official residences, just a 22-mile drive from her official London residence, Buckingham Palace.

You'll see from my [Story Map][UKSM] that we did not tour the inside of the castle on this trip, but that is because I've previously toured the castle eight times. If this is your first trip to the UK, then I absolutely recommend touring Windsor Castle. It is impressive and, truthfully, I prefer its interior to Buckingham Palace.

Things to know about the castle tour: You get to see a lot of rooms and they are decorated as you'd expect from a royal palace---its grandeur upon grandeur. You cannot take pictures inside, but you are free to take pictures outside (and they do sell postcards in the gift shops with pictures of the major sights within and around the castle). If you are interested in English history, do pick up the official souvenir guidebook, but don't expect to be able to read it as you walk on your tour. In my experience, the castle is usually packed, especially on weekends and throughout the summer, so it isn't easy to read when you're packed in with everyone. Definitely take the audio guide, but don't expect it to be super detailed. It gives highlights and overviews of the rooms and features, but it isn't going to point out every great work of art (you'd be there for days if it did). Keep in mind if you skip the audio guide, there really aren't a lot of informational signs in the castle, so you will miss out on valuable information (though this may change as they renovate, so to speak, the visitor experience. More on that below).

Lastly, don't forgot to tour St George's Chapel (which made headlines for Meghan & Harry's wedding). Again, no photos in here, but it is a beautiful church to take in from its incredible vaulted ceilings to the medieval ironwork on a door that survives from Henry III's reign (the 1200s). English history lovers will also want to see the simple grave marker for Henry the VIII's tomb under the center of the Quire.

Tip: Sometimes there is restricted access inside the castle due to events, such as investiture ceremonies. In my opinion, you miss so much of the castle that it isn't worth touring it when they hold these events, so be sure to check the castle's schedule of closures in advance to make sure you won't be disappointed. Windsor Castle, as of the Winter 2020, is transforming the visitor experience, so this may alter aspects of your visit. [Click here for the latest][RCTWindsor-a].

For more, visit the [Royal Collection Trust][RCTWindsor-b].

## Changing of the Guard

Many people know about the Changing of the Guard ceremonies in London (namely at Buckingham Palace, but also at St James' Palace & Wellington Barracks). However, did you know they do a Changing of the Guard ceremony in Windsor? I can't tell you how many times my morning bus ride got held up going into Windsor Town Centre because of the ceremony, but it was often.

If you have the chance, I recommend seeing it since it is a unique experience.

For a schedule, [check here][WindsorGuard]

## The Long Walk

The Long Walk is a beautiful tree-lined avenue that connects Windsor Castle to Windsor Great Park. On weekends, it is flooded with tourists and locals alike. People walk it and jog it and others play with their kids and toss frisbees to their dogs in the green space alongside it. It's a popular gathering area and it's worthwhile to walk at least some of the three miles to get an incredible view of Windsor Castle. If you have time, make the full journey to the Great Park's Copper Horse Statue (where the walk ends). Along the way, you'll pass through part of the Deer Park where you'll have a chance to see the Queen's herd of Red Deer. One time when a dear friend visited me in England, she and I happened upon a gathering of stags grazing here and it was just incredible seeing these animals so close (though still at an appropriate distance for wildlife).

## Windsor Great Park

The parkland is divided up into different areas, including the Savill Gardens, Valley Gardens, Virginia Water, Horse Guards Polo, and the Deer Park. It's a great resource for local residents as it gives them (and gave us) green space to enjoy. As with the Long Walk, you'll see lots of folks running there and walking with their dogs. We loved it and went often. But should you, as a visitor, go? To me, YES! BUT it depends on a few factors. Are you into nature? Enjoy walking? Like photography? If you say yes to these, I think the Great Park is an economical way to spend a lovely day in England.

That's start with one of my favorite attractions: **The Valley Gardens**.

**What is it?** The Valley Gardens comprise 250 acres of woodlands and open valleys. These gardens are beautiful year-round, but I especially love early spring when the magnolias flowers and the valley of daffodils is covered in yellow and white blooms. Later, the spring offers the brilliant colors of azaleas and rhododendrons. Trails will take you through all of these inviting areas. They are showcase seasonal highlights on their [website][WGPVG], so you'll know what to expect when you visit.

**Where is it?** The Valley Gardens are located off Wick Road in Egham. However, the Great Park's website indicates that the lots closet to the Valley Gardens are "members only" lots. This is either a new development or we just disregarded it, because we always paid to park in these lots when we lived in the UK and when we visited in 2018. An annual Windsor Great Park Membership isn't worth the cost of &pound;120 (for a joint membership; &pound;85 for a single) for a one or two-week visit to England. Therefore, I'd have to recommend parking at the Savill Garden car park (off Wick Road). It's about a 20-minute walk to the Valley Gardens from this location. For directions on how to reach the Valley from this carpark, you can stop in the Savill Building and they will provide that information. There will be a fee to park here, as there would be for any of the Great Park's main parking lots.

For more about parking locations in great Park, [click here][WGPParking].

## The Savill Garden

The Savill Garden was created in the 1930s and within its 35 acres you will find the "Hidden Gardens, Spring Wood, the Summer Gardens, the New Zealand Garden, Summer Wood, The Glades, Autumn Wood and the Winter Beds."

My favorite time of year to visit is July and August, when the roses and summer beds are brimming with mature blooms in a kaleidoscope of colors.

**Should I visit?** It depends. Do you love an English Garden? If yes, then Yes! BUT if you must pick only one garden to visit, you may want to explore your options (I've highlighted a few choices within London or nearby below).

If you are doing a garden tour and you'll be in Windsor, then I do recommend touring the Savill Garden. There is great variety here and the gardeners are clearly passionate about their work, because the gardens are beautifully maintained. Again, the highlight for me are their roses, so if you are in town when those are bloom, consider adding the Savill Garden to your itinerary.

**Cons:** I wasn't a fan of their café, but things may have changed over the years.

**Pros:** They have an extensive gift shop and an abundance of parking. The parking fee gets reimbursed if you pay for the gardens. You can also pay to park here and explore other areas of the park (like the Valley Gardens).

## Other Gardens to Consider

If you are staying in London, the world-famous Kew Gardens are accessible by train and they are expansive, offering a variety of different experiences (glass houses, a tree-top walk, and Georgian buildings to tour).

The [Royal Horticultural Society gardens][RHS] are also excellent. RHS Wisley is likely the closet to London and it is exceptional. Wisley is in Woking in the county of Surrey, which is about 34 miles south of central London. In 2014, we visited during their Easter tulip display which boasted 16,000 tulips. It was out of this world beautiful.

Hampton Court Palace [(pdf)][HRP] in London has BEAUTIFUL gardens (I wish they still offered the garden only pass---it was super budget friendly). If you are visiting the palace, don't skip the gardens. They have an awesome maze, beautiful formal gardens (I'm looking at your Pond and Knot gardens), a gorgeous rose garden, and an ancient grape vine that was planted in 1768. How cool is that? Not to mention that the Great Fountain Garden is home to whimsical yews that look as though they come from the pages of a fairytale story. Hampton Court's gardens also have the longest herbaceous mixed flower border in the UK. At 580 meters long, it's a whole lot of flowers to enjoy. Okay. Seriously. Go to the Hampton Court Palace gardens, but be sure to check their website in advance for closures.

Lastly, many of the stately homes throughout England have incredible gardens. If you've never toured an English garden, I truly think everyone should take the opportunity to experience one (unless you are allergic to bees or flowers or the weather churning out lightning bolts and hail).

## Virginia Water

This part of Windsor Great Park is comprised of a man-made lake with a paved path around its perimeter. It's a perfectly nice area to walk, so if you want to get out and enjoy a cheap walk with nice views, this works. You will have to pay for parking in the lots off Blacknest Road or the A30 or folks park on the side of London Road, which I find to be too busy for such a thing and wouldn't risk it in a rental car. If you are limited for time, skip Virginia Water and go for the Valley or Savill Gardens.

## Windsor Great Park Access

Access is admittedly tricky, but because cars can't drive through it, it makes the park the oasis that it is. Foot traffic, horses, and cyclists is about all that you'll see. Occasionally, the White Bus will go by and the few folks driving around are those that live in the park (mostly Crown Estate employees). Explore the [map][WGPIM] to see the where you can park and check out the [White Bus Route 01][WhiteBus]---this grants you access to the park from Sunninghill, Ascot, Sunningdale, and Windsor.

If you are an avid walker, you can absolutely get to the park via the Long Walk. Note, however, that is a 2.5 to 3 mile walk (I've seen various accounts of the actual length) from the castle to the Great Copper Horse, and then beyond that you could spend hours walking through parkland, so making a round trip on foot could truly be an all-day excursion. If you decide to go that route, note that there is a café at the Savill Garden (within the Great Park, but an amusement you must pay for separately, though access to the caf&eacute; and gift shop is free, save for parking if you drive). I'll admit I didn't love the food I got there, but things change, and I'd try it again.

Other dining options for a full-day in the park: The Fox and Hounds restaurant and pub is right off the park on Bishops Gate Road (Approximately 20 minute walk from the Copper Horse Statue); it's roughly 40 minutes (a 2 mile walk) to the Savill Garden caf&eacute; and giftshop from the Copper Horse; Bailiwick pub off Wick Road will take you about an hour to walk to from the Copper Horse, depending on the route you take through the park.

Note that all of these are on the eastern side of the park. Going east, there is an area of the park called The Village, which has a Post Office and General Store, that, during select hours, does sell ice cream (likely in season) and refreshments, but it does have limited hours, which you will want to check in advance. This is about a mile from the Copper Horse and is also about one mile from the car park in Cranbourne Park (directly of Sheet Street/A332). You'll need to cross the A and go through the pedestrian access point at the Cranbourne Gate (near some pink buildings). This is a great option if you want to see The Village, the Copper Horse, or the Queen Elizabeth II Golden Jubilee Statue. Toilet access is another consideration, and aside from the dining options listed and the Savill Garden, I can only think of toilets at certain car parks (like the Valley Gardens). Toilets are marked on the [Crown Estate's park map (pdf)][WGPMap].

The park is large (close to 5,000 acres), so I do recommend planning out a route and seeing where your closet access points are for car parks and bus routes. The Crown Estate, which oversees Windsor Great Park, has a detailed [website with maps][WGPMaps].

## One last tip

Finally, if you are in the area for a few days and have a car, consider making a visit to the [Windsor Farm Shop & Cafe][WFS]. The restaurant has excellent afternoon tea and fab meat pies (chicken and mushroom is my favorite; my husband recommends the steak and ale pie). The shop has standard farm shop offerings, including veg and meat grown on the estate and gift items, like tinned tea and biscuits (cookies). If your accommodations include access to a kitchen, also consider picking up the Royal Feast. This meal includes a whole chicken, sausage, bacon, beans, potatoes, and carrots for £6.95 \*the price and availability may be different now, but it was a tasty all-in-one economical meal option so it's worth looking for if you visit the shop.

[HRP]: https://www.hrp.org.uk/media/2089/hampton-court-gardens-map-web.pdf {.intext-link}
[RBWM]: https://www3.rbwm.gov.uk/info/200122/transport_and_streets/1378/public_conveniences {.intext-link}
[RCTWindsor-a]: (https://www.rct.uk/visit/windsor-castle/practical-information#/) {.intext-link}
[RCTWindsor-b]: https://www.rct.uk/visit/windsor-castle {.intext-link}
[RHS]: https://www.rhs.org.uk/ {.intext-link}
[RoyalAscot]: https://www.ascot.co.uk/horse-races-and-events/royal-ascot/overview {.intext-link}
[UKSM]: https://kateforrestauthor.com/story/map/2018/ {.intext-link}
[WEHTrail]: https://www.windsor.gov.uk/dbimgs/Heritage%20Walking%20Trail3%20(2).pdf {.intext-link}
[WindsorCastleJPG]: https://upload.wikimedia.org/wikipedia/commons/thumb/6/6e/Windsor_Castle-Long_Walk.jpg/640px-Windsor_Castle-Long_Walk.jpg {#scaled-image}
[WindsorGuard]: (https://www.windsor.gov.uk/things-to-do/changing-the-guard-p264351) {.intext-link}
[WFS]: https://www.windsorfarmshop.co.uk/ {.intext-link}
[WGPIM]: http://www.windsorgreatpark.co.uk/en/interactive-map {.intext-link}
[WGPMap]: http://www.windsorgreatpark.co.uk/media/27199/the-royal-landscape-map.pdf {.intext-link}
[WGPMaps]: http://www.windsorgreatpark.co.uk/en/visit/download-maps {.intext-link}
[WGPParking]: http://www.windsorgreatpark.co.uk/en/visit/parking {.intext-link}
[WGPVG]: http://www.windsorgreatpark.co.uk/en/experiences/the-valley-gardens {.intext-link}
[WhiteBus]: https://www.whitebus.co.uk/bus-services/01/ {.intext-link}
