---
title: "Thrifty, Fun Shopping at Tiger"
subtitle: "A companion post to my UK Story Map Day 3 Thursday, 31 May"
author: "Kate Forrest"
date: "March 15, 2020"
abstract: "This blog will overview an economical and quirky shop called Tiger. Tiger stores are all over the UK and they offer travelers some practical and whimsical goods at great prices."
keywords:
- "Travel"
- "Blog"
- "UK"
- "StoryMap"
url: 2020-03-15-thrifty-fun-shopping-at-tiger.html
ishomepage: false
---

<div class="img-fr-wrapper">
![Photo &copy; Richard Humphrey (cc-by-sa/2.0)](assets/images/geograph-4028416-by-Richard-Humphrey.jpg){#scaled-image}
</div>

For the full story, check out the Story Map [here][UKSM].

I went to my first Tiger in Richmond, a neighborhood in West London that borders Kew Gardens. It was handy to the train station, so I stopped in to kill time one day and I've been hooked ever since. It's like if Ikea had a dollar store. As a non-shopper, it was a strange experience for me to walk in this store and instantly fall in love, but that is exactly what happened. It's beyond fun and I can spend a solid 30 minutes in there happily browsing everything from storage and kitchenware to greeting cards and spices. Even if you aren't a shopper and you aren't keen on knick-knacks, I still recommend this shop to fellow travelers.

Here's a few examples of why. Our luggage didn't all fit in the trunk and, having rented the cheapest car I could for our 25-day adventure, our backseat windows weren't tinted, so I wanted a blanket to afford some privacy to our goods. I picked up a super cute lemon themed throw blanket for like &pound;8 (I don't have the receipt handy, but that sounds right); a cheap fabric lunchbox that we used as a small drink cooler (also great for packing up stuff before we left); extra luggage tags; reusable shopping totes for &pound;2; some quirky (affordable) books and toys as unique souvenirs for our nieces, and an umbrella (which we needed, but I didn't want to pack from the States to save space. I found umbrellas in Windsor, but they were too expensive for something that just needed to be serviceable for a month. I found a lemon themed one (to match my blanket), and despite it being a thrifty buy, it still holds up with frequent use over a year and a half later.

For how silly some of it looks, the majority of the merchandise (and arguably all of it) is functional. There is plenty in Tiger that a traveler would find useful, so if you see the Tiger sign, stop in and explore. They have locations throughout the UK. On our trip in May, we stopped at the Tiger in Reading and the one in Richmond (London). For more info, check out their [UK website][UKFT].

[UKSM]: https://kateforrestauthor.com/story/map/2018/ {.intext-link}
[UKFT]: https://uk.flyingtiger.com/en-GB {.intext-link}
