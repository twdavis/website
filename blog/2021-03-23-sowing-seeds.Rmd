---
title: "Sowing Seeds & Gloucester's Daffodil Festival"
subtitle: "Week 4 in the garden"
author: "Kate Forrest"
date: "March 23, 2021"
abstract: "Today I'll recap the projects we worked on last week through today and my driving tour of Gloucester County's famous Daffodil Festival."
keywords:
- "Blog"
- "Garden"
url: 2021-03-23-sowing-seeds.html
ishomepage: false
---

> "Daffodils come before the swallow dares, and take the winds of March with beauty." <br />&mdash; Shakespeare

Hey, all! Welcome back to my gardening blog.
Today I'll recap the projects we worked on last week through today and our driving tour of Gloucester County's famous Daffodil Festival.

## Plants & Seeds

Starting on Wednesday, March 17<sup>th</sup>, we collected a bunch of plants from Penn Farm/Norma's Produce via the Williamsburg Farmer's Market.
Our buys included: Chives, Julep Mint, Lavender Thyme, Pennyroyal mint (repels insects), peppermint, pineapple sage (which we grow as a shrub for its late summer vibrant red trumpet-like flowers), and, as a treat for myself, a mixed succulent mini hanging basket.
When the weather allows, I'll add the succulents to our flagstone rock wall, where we had great success last summer with our hen & chicks.

The evening of Thursday, March 18<sup>th</sup>, I started more seeds indoors.
The weather was wild---it warmed up and several thunderstorms came through with a few heavy downpours---so it was good to be working inside.
I reused a seed starting tray from last year (that I refilled with my seed sowing medium from Burpee last weekend).
The tray has 104 plugs, so plenty of room to get the following all started:

-   Phlox (Blushing Bride; Burpee) -- 2 rows.
-   Coleus (Chocolate Mint; Burpee) -- 2 rows.
-   Coleus (Chocolate Covered Cherries; Burpee). All ten seeds sown.
-   Salvia (Victoria; Burpee).
-   Mexican Feather Grass (Hayfield).

In my new XL seed starting trays from Burpee (8 plugs), I sowed more salvia and four Lavender blooming coins (Love Wild Design).
I placed all of the trays and the new herbs and succulents on shelves in our south-facing guest bedroom window alongside our pineapple plant that's been successfully overwintering inside (despite how rarely we remember to water it).

In the kitchen, I sowed Jalape&ntilde;o Peppers (Ferry \~ Morse) in small seed-starting terra cotta pots that I sat in our south facing window.

No signs of life yet from the lettuce or kale seeds I sowed last weekend.

## Daffodil Festival

On Saturday, March 20<sup>th</sup> we took a mini-road trip to Gloucester County to enjoy its famous Daffodil Festival.
The main vendor event (scaled down because of the pandemic), will be held this Saturday and Sunday, March 27<sup>th</sup> &amp; 28<sup>th</sup>, but we were there for the self-guided driving tour.
Starting out, it was slim pickings, with blooms just opening at the magnificent Abingdon Episcopal Church.
In a week or two, it will be spectacular.
Undeterred, we carried on into Gloucester's historic town center and we weren't disappointed---Gloucester's Historic Court Circle was surrounded by blooming daffodils.
It was just beautiful.

![Gloucester's Historic Court Circle.](assets/images/gloucester_town_980x557.png)

Gloucester is a charming town with a good many shops currently open on Main Street, so I absolutely recommend spending time in town to enjoy the offerings if you ever visit (for the festival or for its many historic properties, as we have done).
I also recommend stopping in for a pint at Gloucester Brewing Company (good beer), which frequently hosts food trucks too.
We took a stroll through town to enjoy all the plantings and building murals before hopping back in the car.

<div class="img-fr-wrapper">
![The Staghorn fern in Brent & Becky's bulb shop.](assets/images/staghorn_fern_500x500.png)
</div>

From town, we headed up to Brent & Becky's Bulbs.
I've shopped their catalog since moving to Williamsburg, buying everything from bareroot lavender to tulips, but I hadn't yet visited their bulb shop.
While their daffodil displays were just starting to flower (as was the case in most locations on the tour), it was nice to visit their shop and admire all the bulbs and plants for sale, including staghorn ferns (which I adore).
They have one gigantic one hanging from the ceiling---just incredible.
They are the neatest ferns.
I was introduced to them by a friend many years ago and now I always look for them when I tour gardens.
I haven't taken the plunge to grow one myself, but it's definitely on my "Plants to grow in my lifetime" bucket list.

While in the shop, I picked up some white daffodils (which I want to start introducing into our solid yellow collection) so I can now proudly say I am the owner of some Gloucester daffodils.
I also got some yellow hyacinths to expand on our hyacinth collection.

From there, we made one more stop at Ware Episcopal Church and were treated to the most beautiful sight: a forest floor blanketed with blooming daffodils.
It was something from a fairytale.
I took about fifty photographs, but I wanted to share the video I took as we left.
It's going in my "carpet bag" of cherished garden memories right alongside seeing the Welsh Bodnant Garden's laburnum arch in peak bloom.

<div class="iframe-container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/JJcwUUR362o" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

## A few hours of daylight

When we got home, we still had a few hours of daylight left (loving these longer spring days), so we were on a mission to get a few more items ticked off the box.
Quick wardrobe change into yard clothes and we set about topping off the raised beds.
Those heavy rainstorms from Thursday brought down the soil level a few inches, so we wanted to get those topped off before sowing the peas.
Once we'd topped off the beds, we sowed two rows of sugar snap peas.
We also filled up a whiskey barrel with potting soil and I strategically "dumped" a bunch of poppy seeds on the surface, then used the mist setting on the hose to water them in since they need sunlight to germinate.
I'm told poppies can lay dormant for years, so who knows if we'll get any this year or not.
As with most things, it's an experiment.

We then transplanted a bunch of daffodils that didn't flower this year, likely due to overcrowding.
We got those divided up and added a dozen or so to our redbud bed, which I'd love to make into a showstopper bulb bed someday.

## The Battle with the Chainsaw

Sunday started with the task of getting the chainsaw cleaned up and functional.
Three hours later, we discovered a leak and that didn't have a quick remedy so we conceded defeat for the day to that endeavor, and went old-school to manually saw and chop down the rest of a dying red-tip, whose remains have haunted us since the autumn.
It was just one of those jobs we never got to finish, so it was wonderful to see that project done.
There's still a stump, but it's in a tricky position with its root system likely part of our flagstone rock wall, so the stump will stay put and either be planted in or planted around or both.

We then had the issue of the apple tree bed washout to contend with.
It had rained on the bed after we cleared it last weekend, and despite increasing the soil level several inches, we didn't get a washout, so we thought we were safe.
After the heavy storms Thursday, we got washout in several areas.
We've since realized, we either need plastic edging (which we don't like) or we need a short retaining wall.
After looking at our options, we currently plan to go with 4-inch-tall flagstone retaining wall blocks.
We'll be sourcing those this week and hopefully getting that project started either this weekend or Easter weekend.
In the meantime, we've added the soil back in and will be keeping our fingers crossed we don't have another washout before we can get that wall installed.

We also planted out our new white daffodils and hyacinths in the apple tree and lantana beds and called it a day.

## Lavender and such

Tonight before dark, we ran out to get a few jobs done.
First, I cut back the transplanted lavender by one third because new growth is coming in on all of them, so I needed to get that done.
We also cleaned up some of our front beds and trimmed a few more plants, including our Rose of Sharon.
My husband also cut down a dead branch on one of the three dying red tip trees that need to be removed this spring.
It was nice just getting out in the yard for an hour on a weeknight, which will hopefully happen more frequently with the longer days and the warmer weather.

## What's in bloom in our yard and Williamsburg?

### Our yard

<div class="img-fr-wrapper">
![One of our new daffodils.](assets/images/our_bloom_500x500.png)
</div>

-   Lenten roses
-   Daffodils are in their glory, with new varieties opening
-   Purple and yellow crocuses (the white have now finished)
-   Camellias (really churning out blooms right now; just beautiful)
-   Violas & Pansies

### Around town

All of the above and:

-   Hyacinths
-   Forsythia
-   Primroses
-   Periwinkle
-   Lamium purpureum (purple deadnettle) is abundant along roadsides and in fields
-   Various ground covers, like buttercups
-   Flowering trees

## What else is doing?

-   Our hydrangea is up and leafing out
-   Roses and trumpet vine has all leafed out
-   Astilbes (which I purchased bareroot in February) are several inches tall
-   Redbud and apple are budding, with leaves opening on apple tree
-   The understory in the forest is greening up
-   Tulips are getting tall

Alright, all.
That's a wrap.
Thanks for stopping by to catch up on our gardening adventures.
Wishing you all a very Happy Spring!
I'll be back with more updates next Tuesday, the 30<sup>th</sup>.

\~Kate
